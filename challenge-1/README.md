# Instructions

This first challenge involves changing your fonts and colours. Check the image to see what your page should look like!

Link an external stylesheet.

### Hints:

- You can use the following fonts list for help: http://www.w3.org/Style/Examples/007/fonts
- You forgot how to link an external stylesheet? This can help : <link rel="stylesheet" href="external-stylesheet.css">
- Make sure to check all the property available to an unordered list.